Origin: upstream, ce9d7c00cb29a4823bfbba0da78c408badc6a1f5
From: Guillaume Ayoub <guillaume@courtbouillon.org>
Date: Sat, 16 Mar 2024 10:12:36 +0100
Subject: Use ruff instead of flake8 and isort

---
 .github/workflows/tests.yml |  4 +---
 cssselect2/compiler.py      |  2 +-
 cssselect2/tree.py          |  7 +++----
 docs/contribute.rst         |  9 +++------
 pyproject.toml              | 11 +++++++----
 tests/test_cssselect2.py    |  3 ++-
 6 files changed, 17 insertions(+), 19 deletions(-)

diff --git a/.github/workflows/tests.yml b/.github/workflows/tests.yml
index f532d76..3a9061f 100644
--- a/.github/workflows/tests.yml
+++ b/.github/workflows/tests.yml
@@ -28,6 +28,4 @@ jobs:
       - name: Launch tests
         run: python -m pytest
       - name: Check coding style
-        run: python -m flake8 --exclude tests/css-parsing-tests
-      - name: Check imports order
-        run: python -m isort . --check --diff
+        run: python -m ruff check
diff --git a/cssselect2/compiler.py b/cssselect2/compiler.py
index b03de7e..b361bae 100644
--- a/cssselect2/compiler.py
+++ b/cssselect2/compiler.py
@@ -398,7 +398,7 @@ def _compile_node(selector):
             # Matches if a positive or zero integer n exists so that:
             # x = a*n + b-1
             # x = a*n + B
-            B = b - 1
+            B = b - 1  # noqa: N806
             if a == 0:
                 # x = B
                 return f'({count}) == {B}'
diff --git a/cssselect2/tree.py b/cssselect2/tree.py
index 9b2f158..890d56f 100644
--- a/cssselect2/tree.py
+++ b/cssselect2/tree.py
@@ -10,7 +10,7 @@
     cached_property = functools.cached_property
 else:
     # Python 3.7
-    class cached_property:
+    class cached_property:  # noqa: N801
         # Borrowed from Werkzeug
         # https://github.com/mitsuhiko/werkzeug/blob/master/werkzeug/utils.py
 
@@ -132,8 +132,7 @@ def ancestors(self):
         """
         if self._ancestors is None:
             self._ancestors = (
-                () if self.parent is None else
-                self.parent.ancestors + (self.parent,))
+                () if self.parent is None else (*self.parent.ancestors, self.parent))
         return self._ancestors
 
     @property
@@ -147,7 +146,7 @@ def previous_siblings(self):
         if self._previous_siblings is None:
             self._previous_siblings = (
                 () if self.previous is None else
-                self.previous.previous_siblings + (self.previous,))
+                (*self.previous.previous_siblings, self.previous))
         return self._previous_siblings
 
     def iter_ancestors(self):
diff --git a/docs/contribute.rst b/docs/contribute.rst
index b7298b5..c6ca1a2 100644
--- a/docs/contribute.rst
+++ b/docs/contribute.rst
@@ -47,15 +47,12 @@ You can launch tests using the following command::
 
   venv/bin/python -m pytest
 
-cssselect2 also uses isort_ to check imports and flake8_ to check the coding
-style::
+cssselect2 also uses ruff_ to check the coding style::
 
-  venv/bin/python -m isort . --check --diff
-  venv/bin/python -m flake8 --exclude tests/css-parsing-tests
+  venv/bin/python -m ruff check
 
 .. _pytest: https://docs.pytest.org/
-.. _isort: https://pycqa.github.io/isort/
-.. _flake8: https://flake8.pycqa.org/
+.. _ruff: https://docs.astral.sh/ruff/
 
 
 Documentation
diff --git a/pyproject.toml b/pyproject.toml
index 9d8f3a1..6930ef4 100644
--- a/pyproject.toml
+++ b/pyproject.toml
@@ -40,7 +40,7 @@ Donation = 'https://opencollective.com/courtbouillon'
 
 [project.optional-dependencies]
 doc = ['sphinx', 'sphinx_rtd_theme']
-test = ['pytest', 'isort', 'flake8']
+test = ['pytest', 'ruff']
 
 [tool.flit.sdist]
 exclude = ['.*']
@@ -53,6 +53,9 @@ include = ['tests/*', 'cssselect2/*']
 exclude_lines = ['pragma: no cover', 'def __repr__', 'raise NotImplementedError']
 omit = ['.*']
 
-[tool.isort]
-default_section = 'FIRSTPARTY'
-multi_line_output = 4
+[tool.ruff.lint]
+select = ['E', 'W', 'F', 'I', 'N', 'RUF']
+ignore = ['RUF001', 'RUF002', 'RUF003']
+
+[tool.ruff.lint.extend-per-file-ignores]
+'docs/example.py' = ['I001']
\ No newline at end of file
diff --git a/tests/test_cssselect2.py b/tests/test_cssselect2.py
index c0c9f74..f4cc24e 100644
--- a/tests/test_cssselect2.py
+++ b/tests/test_cssselect2.py
@@ -4,10 +4,11 @@
 
 """
 
-import xml.etree.ElementTree as etree
+import xml.etree.ElementTree as etree  # noqa: N813
 from pathlib import Path
 
 import pytest
+
 from cssselect2 import ElementWrapper, SelectorError, compile_selector_list
 
 from .w3_selectors import invalid_selectors, valid_selectors
